'use strict';

var gulp = require('gulp');
var browserSync = require('browser-sync');
var nodemailer = require('nodemailer');
var fs = require('fs');
var config = require('./configs/config');
var mailOptions = {};

var transporter = nodemailer.createTransport({
    service: config.sender.type,
    auth: {
        user: config.sender.user,
        pass: config.sender.pass
    }
});

var options = {
    data: 'app/' + config.workingFolder + '/layout/data/*.{json,yml}',
    layouts: 'app/' + config.workingFolder + '/layout/',
    partials: 'app/' + config.workingFolder + '/includes/*.hbs'
};

gulp.task('serve', function () {
  browserSync({
    notify: false,
    logPrefix: 'WSK',
    server: ['app/'+ config.workingFolder]
  });

  gulp.watch('app/' + config.workingFolder +'/img/**/*', browserSync.reload);
  gulp.watch('app/' + config.workingFolder +'/*.html', browserSync.reload);
});

gulp.task('pp', function () {
    browserSync({
        notify: false,
        logPrefix: 'WSK',
        server: ['app/' + config.workingFolder]
    });
});

gulp.task('send-test', function () {

    fs.readFile( config.litmus.html, function (err, data) {
        if (err) throw err;

        mailOptions.default = config.litmus;
        mailOptions.default.html = data;

        transporter.sendMail(mailOptions.default, function(error, info){
            if(error){
                console.log(error);
            }else{
                console.log('Message sent: ' + info.response);
            }
        });
    });
});

