

var config = {
    workingFolder: 'ad-temp',
    litmus: {
        from: ' Local sender ',
        to: 'test@test.com',
        subject: 'Test email',
        html: 'app/' + config.workingFolder + '/index.html'
    },
    sender: {
        type: 'Gmail',
        user: 'sender@gmail.com',
        pass: 'sender_pass'
    }
};

module.exports = config;